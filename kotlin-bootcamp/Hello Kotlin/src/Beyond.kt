import Aquarium.Fish

fun pairs() {

    val pair = "first" to "second"
    val paris = ("first" to "second") to ("first" to "second")

    println(pair.first)
}

fun packPair(): Pair<String, String> {
    return "first" to "second"
}

fun unpackPair() {
    val (first, second) = packPair()
    val string = packPair().toString()
    val list = packPair().toList()
}

fun packTriple(): Triple<String, String, String> {
    return Triple(first="first", second="second", third="third")
}

fun collections() {
    val c = mutableListOf("a", "b", "c");
    c.add("d")
    c.remove("a")
    c.contains("b")

    println(c.subList(2, c.size))
    listOf(1, 5, 3).sum()
    listOf("a", "b", "c").sumBy { it.length }

    val m = mapOf("a" to "b", "c" to "d")
    m.get("a")
    m["a"] // sugar
}

// constants
const val i = 3 // Set at compile time
val j = 5 // Set at runtime

class MyClass {
    companion object {
        const val CONSTANT = "const" // lazy
    }
}

// extension functions

fun String.hasSpaces() = find { it == ' '} != null

fun extensionsExample() {
    "Hello world!".hasSpaces()
}

// generics

open class Item

class MyList<T : Item> {
    fun get(pos: Int): T {
        TODO("implement")
    }
    fun addItem(item: T) {
        TODO("implement")
    }

    // generic function
    inline fun <reified R: Item> isCertainItem(item: Item) = item is R
}

fun checkSomething(s: String) {
    check(s == "Hello world!") { "String must be 'Hello world!'" }
}

// annotations
annotation class ImAClass

@ImAClass class MyNewClass

fun reflections() {
    val classObj = MyNewClass::class

    for (annotation in classObj.annotations) {
        println(annotation.annotationClass.simpleName)
    }
}

// labeled breaks

fun labels() {
    outer@ for (i in 1..100) {
        for (j in 1..100) {
            if (i > 10) break@outer // resume at the label
        }
    }
}

// higher order function
fun example() {
    with ("hello") { capitalize() }
}

// runtime will call myWith
fun myWith(name: String, block: String.() -> Unit) {
    name.block()
}

// compiler will replace myWith
inline fun myWith2(name: String, block: String.() -> Unit) {
    name.block()
}

fun myWith3 (i: Any, block: Any.() -> Unit) {
    i.block()
}

class MyFish(var name: String)

fun others() {
    val s = "Hello world"

    s.run { toString() } // returns result
    s.apply {  } // returns object

    val fish = MyFish(name="Fishy").apply{name="Shark"}

    // returns modified object
    val result = fish.let { it.name.capitalize() }
        .let { it + "fish" }
        .let { it.length }
        .let { it + 31 }
}


// SAM: Single Abstract Method

interface Runnable {
    fun run()
}

interface Callable<T> {
    fun call(): T
}


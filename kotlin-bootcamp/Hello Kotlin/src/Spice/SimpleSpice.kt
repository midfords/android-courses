package Spice

class SimpleSpice(val name: String, var level: String) {
    val heat: Int
            get() {
                return when(level) {
                    "mild" -> 5
                    else -> 0
                }
            }
}

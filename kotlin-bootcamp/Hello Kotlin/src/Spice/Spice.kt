package Spice

abstract class Spice(val name: String, val level: String, color: SpiceColor):
    SpiceColor by color {

    val heat: Int
        get() {
            return when(level) {
                "low" -> 2
                "mild" -> 5
                "medium" -> 7
                "spicy" -> 9
                else -> 0
            }
        }

    abstract fun prepareSpice()
}

data class SpiceContainer(val spice: Spice) {
    val label = spice.name
}

class Curry(name: String = "Curry", level: String = "mild"): Spice(name, level, YellowColor), Grinder {

    override fun prepareSpice() {
        grind()
    }
}

class Salt(level: String = "low"): Spice("Salt", level, WhiteColor) {

    override fun prepareSpice() { }
}

interface Grinder {

    fun grind() {
        println("Grinding..")
    }
}

interface SpiceColor {

    val color: String
}

object YellowColor: SpiceColor {
    override val color: String
        get() = "yellow"
}

object WhiteColor: SpiceColor {
    override val color: String
        get() = "white"
}

fun makeSalt() = Salt()

val spices = listOf(
    SpiceContainer(Curry("Red Curry", "medium")),
    SpiceContainer(Curry("Green Curry", "spicy"))
)

fun main() {
    for(i in spices) println(i.label)
}

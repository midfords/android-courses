import java.util.*

fun helloWorld() {
    println("Hello World")
}

fun hello(name: String) {
    println("Hello, $name")
}

fun dayOfWeek() {
    println("What day is it today?")
    val day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
    println( when(day) {
        1 -> "Sunday"
        2 -> "Monday"
        3 -> "Tuesday"
        4 -> "Wednesday"
        5 -> "Thursday"
        6 -> "Friday"
        else -> "Saturday"
    })
}

fun isMorning() : Boolean {
    return Calendar.getInstance().get(Calendar.AM_PM) == 0
}

fun greeting(name: String) {
    println("Good ${if (isMorning()) "morning" else "night"} $name.")
}

fun randomDay() : String {
    val week = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    return week[Random().nextInt(7)]
}

fun fishFood(day: String): String {
    return when(day) {
        "Sunday" -> "plankton"
        "Monday" -> "flakes"
        "Tuesday" -> "pellets"
        "Wednesday" -> "redworms"
        "Thursday" -> "granules"
        "Friday" -> "mosquitoes"
        else -> "lettuce"
    }
}

fun feedTheFish() {
    val day = randomDay()
    val food = fishFood(day)
    println("Today is $day and the fish eat $food")

    if (shouldChangeWater(day))
        println("Change the water today.")

    dirtyProcessor()
}

fun swim(speed: String = "fast") {
    println("Swimming $speed")
}

fun callSwim() {
    swim() // Default
    swim("slow") // Positionally
    swim(speed="slow") // Named
}

fun isTooHot(temp: Int) = temp > 30
fun isDirty(dirty: Int) = dirty > 30
fun isSunday(day: String) = day == "Sunday"

fun getDirtySensorReading() = 20

fun shouldChangeWater(
    day: String,
    temp: Int = 22,
    dirty: Int = getDirtySensorReading()): Boolean {

    return when {
        isTooHot(temp) -> true
        isDirty(dirty) -> true
        isSunday(day) -> true
        else -> false
    }
}

fun lambdas() {
    val swim = { println("Swim \n") }
    swim()

    val waterFilter: (Int) -> Int = { dirty: Int -> dirty / 2}
    waterFilter(10)

    val waterFilterInfered: (Int) -> Int = { dirty -> dirty / 2}
    waterFilter(10)
}

// Higher order functions
fun updateDirty(dirty: Int, operation: (Int) -> Int): Int {
    return operation(dirty)
}

val waterFilter: (Int) -> Int = { dirty: Int -> dirty / 2}
fun feedFish(value: Int): Int { return 1 }

fun dirtyProcessor() {
    updateDirty(20, waterFilter) // Pass lambda
    updateDirty(20, ::feedFish) // Pass named function reference
    updateDirty(20) { dirty ->
        dirty + 50
    } // Define paramaterized function during call
}

fun main(args: Array<String>) {
    feedTheFish()

    val day = randomDay()
    shouldChangeWater(day, 20, 50)
    shouldChangeWater(day)
    shouldChangeWater(day, dirty=50)

    val letters = listOf("a", "b", "c")
    letters.filter { it.startsWith('a') || it.endsWith('b') }
    letters.sortedBy { it.length }
    letters.take(3)
}



fun getBirthday(): Int {
    print("Enter your birthday: ")
    return readLine()?.toIntOrNull() ?: 1
}

fun getFortuneCookie(birthday: Int): String {
    return when(birthday) {
        in 1..7 -> "You will have a great day!"
        8 -> "Things will go well for you today."
        9, 10 -> "Enjoy a wonderful day of success."
        in 11..14, 16 -> "Be humble and all will turn out well."
        17 -> "Today is a good day for exercising restraint."
        in 18..30 -> "Take it easy and enjoy life!"
        else -> "Treasure your friends because they are your greatest fortune."
    }
}

fun main() {
    val day = getBirthday()
    val msg = getFortuneCookie(day)

    println("Your fortune is: $msg")
}
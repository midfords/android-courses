package Aquarium

fun main() {
    delegate()
}

fun delegate() {
    val pleco = Plecostomus()
    println("Fish has color ${pleco.color}")
    pleco.eat()
}

interface FishAction2 {
    fun eat()
}

interface FishColor {
    val color: String
}

// Class composition
// Interface delegation with 'by' keyword
// Good for listeners?
class Plecostomus2(fishColor: FishColor = GoldColor):
    FishAction2 by PrintingFishAction("a lot of algae"),
    FishColor by fishColor

// Singleton
object GoldColor: FishColor {
    override val color = "gold"
}

class PrintingFishAction(val food: String): FishAction2 {
    override fun eat() {
        println(food)
    }
}
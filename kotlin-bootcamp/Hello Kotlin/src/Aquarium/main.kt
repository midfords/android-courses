package Aquarium

fun buildAquarium() {

    val myAquarium = Aquarium()

    println("Length: ${myAquarium.length} " +
            "Width: ${myAquarium.width} " +
            "Height: ${myAquarium.height}")

    myAquarium.height = 200

    println("Height is now: ${myAquarium.height}")
}

fun main() {
    buildAquarium()
}
package Aquarium

abstract class AquariumFish {
    abstract val color: String
}

interface FishAction {

    fun eat() {
        println("default eat.")
    }
}

class Shark: AquariumFish(), FishAction {
    override val color = "grey"

    override fun eat() {
        println("hunt and eat fish.")
    }
}

class Plecostomus: AquariumFish(), FishAction {
    override val color = "black"

    override fun eat() {
        println("munch on algae")
    }
}

fun feedFish(fish: FishAction) {
    fish.eat()
}

fun makeFish() {
    val shark = Shark()
    val pleco = Plecostomus()

    println("Shark: ${shark.color} \n Plecostomus ${pleco.color}")
    feedFish(shark)
    feedFish(pleco)
}

fun main() {
    makeFish()
}
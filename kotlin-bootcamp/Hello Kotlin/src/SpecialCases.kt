
// Singleton, only 1 instantiated
object MobyDickWhale {

    val author = "Herman Melville"

    fun jump() {

    }
}

// Singleton, 1 of each item instantiated (RED, GREEN, BLUE).
enum class Color(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF)
}

// Can only be extended by classes sharing its file.
// Known statically at compile time.
sealed class Sealed

sealed class Seal

class SeaLion: Seal()
class Walrus: Seal()

fun matchSeal(seal: Seal): String {
    return when(seal) {
        is Walrus -> "walrus"
        is SeaLion -> "sea lion"
    }
}